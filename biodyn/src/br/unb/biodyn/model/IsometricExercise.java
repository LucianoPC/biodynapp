package br.unb.biodyn.model;

import android.os.CountDownTimer;
import android.util.Log;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.ui.StartTimer;

public class IsometricExercise extends Exercise {

	/********************************** CONSTANTS ***********************************/
	private static final String TAG = "IsometricExercise";
	private static final float PERCENT_ERROR_MAXIMUM = 1.1f;
	private static final float PERCENT_ERROR_MINIMUM = 0.9f;
	private static final int ONE_SECOND = 1000;
	private static final long CONFIRM_TIMER_DURATION = 4000;

	private static final float DEFAULT_MAXIMUM_WEIGHT = 6;
	private static final float DEFAULT_MINIMUM_WEIGHT = 4;
	private static int DEFAULT_REPETITIONS = 10;
	private static int DEFAULT_SERIES = 5;
	private static final int DEFAULT_REST_TIME = 30000;
	private static final long DEFAULT_HOLD_TIME = 5000;

	/********************************** CLASS FIELDS ***********************************/

	private float realWeight;
	private boolean checkMaximumWeight;
	private boolean checkMinimumWeight;
	private boolean restTime = false;
	private ConfirmTimer myConfirmTimer;

	Timer time = new Timer();
	StartTimer startTimer = new StartTimer(DEFAULT_HOLD_TIME, DEFAULT_HOLD_TIME);

	/********************************** CLASS METHODS ***********************************/

	public IsometricExercise(ExerciseData data) {
		super(data);
		this.initialRepetitions = repetitions;
	}

	public IsometricExercise() {
		this(new ExerciseData(DEFAULT_MAXIMUM_WEIGHT, DEFAULT_MINIMUM_WEIGHT,
				DEFAULT_REPETITIONS, DEFAULT_SERIES, DEFAULT_REST_TIME,
				DEFAULT_HOLD_TIME));
	}

	@Override
	public boolean checkRepetition(float data) {
		realWeight = data;
		
		LogFormat.setLogMessage(LogFormat.EXERCISE, "checkRangeWeightOfExercise(): " + checkRangeWeightOfExercise());
		if(checkRangeWeightOfExercise()) {
			LogFormat.setLogMessage(LogFormat.EXERCISE, "entrou no IF: ");
			Communicator.getInstance().sendToUI(Communicator.FLAG_START_HOLD_TIME, this);
		}
		else{
			Communicator.getInstance().sendToUI(Communicator.FLAG_STOP_HOLD_TIME, this);
		}
		
		return checkRangeWeightOfExercise() && holdTime == 0;
		
	}

	@Override
	public void reset() {
		repetitions = this.initialRepetitions;
		series = this.initialSeries;
		Communicator.getInstance().sendToUI(
				Communicator.FLAG_SERIES | Communicator.FLAG_REPETITIONS
						| Communicator.FLAG_LIMITS | Communicator.FLAG_HOLD_TIME, this);
		LogFormat.setLogMessage("Reset", "is reseting the values");
	}

	@Override
	public void countDownRepetitions() {
		repetitions--;

		if (myConfirmTimer != null) {
			myConfirmTimer.cancel();
			myConfirmTimer = null;
		}

		if (myConfirmTimer == null)
			myConfirmTimer = new ConfirmTimer();
		
		if (restTime) {
			restTime = false;
			Communicator.getInstance().sendToUI(Communicator.FLAG_STOP_TIMER,
					this);
		}

		myConfirmTimer.start();

		Communicator.getInstance()
		.sendToUI(Communicator.FLAG_REPETITIONS, this);
	}

	@Override
	public void countDownSeries() {
		this.series--;

		if (series == 0) {
			reset();
			stop();
		} else {
			Communicator.getInstance().sendToUI(Communicator.FLAG_START_TIMER,
					this);

			restTime = true;
		}
		repetitions = initialRepetitions;

		Communicator.getInstance().sendToUI(
				Communicator.FLAG_SERIES | Communicator.FLAG_REPETITIONS, this);
	}

	public boolean checkRangeWeightOfExercise() {
		checkMinimumRange();
		checkMaximumRange();
		LogFormat.setLogMessage(LogFormat.EXERCISE, "Check maximum weight: " + checkMaximumWeight);
		LogFormat.setLogMessage(LogFormat.EXERCISE, "Check minimum weight: "+ checkMinimumWeight);
		return checkMaximumWeight && checkMinimumWeight;
	}

	private boolean checkMinimumRange() {
		if (realWeight >= minimumWeight * PERCENT_ERROR_MINIMUM) {
			this.checkMinimumWeight = true;
			Log.d("debug", "checkFirstMinimumValue");
			return true;
		} else {
			this.checkMinimumWeight = false;
			return false;
		}
	}

	private boolean checkMaximumRange() {
		if (realWeight <= maximumWeight * PERCENT_ERROR_MAXIMUM) {
			this.checkMaximumWeight = true;
			Log.d("debug", "checkMaximumRange");
			return true;
		} else {
			this.checkMaximumWeight = false;
			return false;
		}
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}


	private class ConfirmTimer extends CountDownTimer {

		public ConfirmTimer() {
			super(getHoldTime() * 2, getHoldTime() * 2);
		}

		@Override
		public void onTick(long millisUntilFinished) {
		}

		@Override
		public void onFinish() {
			LogFormat.setLogMessage(LogFormat.MERGE, "ConfirmTimer - onFinish");
			countDownSeries();
			Communicator.getInstance().sendToUI(Communicator.FLAG_START_TIMER, IsometricExercise.this);
		}
	}
	
}
