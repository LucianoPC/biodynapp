package br.unb.biodyn.model;

public interface BioFeedback {
	
	public void start(long time);
	
	public void stop();
}
