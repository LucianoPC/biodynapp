package br.unb.biodyn.ui;

import android.content.Context;
import android.util.AttributeSet;
import br.unb.biodyn.Communicator;
import br.unb.biodyn.model.ExerciseData;
import br.unb.biodyn.model.LogFormat;

public class HoldTimerUI extends TimerUI {

	public HoldTimerUI(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

//	@Override
//	public void startTimer(long time) {
//		if(getActualTime() <= 0)
//			super.startTimer(time);
//	}
	
	@Override
	public void onTick(StartTimer startTimer, long millisUntilFinished) {
		super.onTick(startTimer, millisUntilFinished);

		ExerciseData exerciseData = new ExerciseData();

		exerciseData.setHoldTime(getActualTime());

		Communicator.getInstance().sendToExerciseService(
				Communicator.FLAG_HOLD_TIME, exerciseData);
	}
	
	@Override
	public void onFinish(StartTimer startTimer) {
		LogFormat.setLogMessage(LogFormat.MERGE, "HoldTimerUI - onFinish");
		Communicator.getInstance().sendToExerciseService(Communicator.FLAG_FORCE_REPETITION, null);
		Communicator.getInstance().sendToBioFeedback(
				Communicator.BioFeedbackServiceActions.ACTION_START,
				Communicator.BioFeedbackServiceActions.FLAG_VIBRATION, 1000);
	}

}
